const record = require('node-record-lpcm16');
const Detector = require('snowboy').Detector;
const Models = require('snowboy').Models;
const FileWriter = require('wav').FileWriter;
const models = new Models();
const net = require('net');
const path = require('path');
const cp = require('child_process');
const request = require('superagent');
const beginSound = path.join(__dirname, "resources/sounds/stt_begin.mp3");
const endSound = path.join(__dirname, "resources/sounds/stt_end.mp3");

const port = 1201;
const sockets = {};

//Create STT Server
net.createServer(function (socket) {
    const id = socket.remoteAddress + ":" + socket.remotePort;
    console.log(`Got a new connection: ${id}`);
    socket.on('data', function (message) {
        // console.log(message.toString())
    });
    socket.on('error', function () {
    });
    socket.on('close', function () {
        if (id in sockets) {
            delete sockets[id];
        }
    });
    sockets[id] = socket;
}).listen(port, '127.0.0.1', function () {
    console.log(`STT server listening on port ${port}.`)
});

// Create a instance of Snowboy models
models.add({
    file: 'resources/models/alexa.umdl',
    sensitivity: 0.5,
    hotwords: 'alexa'
});

// Create Detector
const detector = new Detector({
    resource: "resources/common.res",
    models: models,
    audioGain: 2.0,
    applyFrontend: true
});

const time = function () {
    return Math.floor(Date.now() / 1000);
};

const timeout = 5;
let catchWord = false;
let startTime = 0;
let bufferArray = [];

function recognize() {
    // Play the endSound
    cp.spawn('cvlc', ['--play-and-exit', endSound]);
    const buffer = Buffer.concat(bufferArray);
    // Write buffer to the output file
    const outputFileStream = new FileWriter('output.wav', {
        sampleRate: 16000,
        channels: 1
    });
    outputFileStream.write(buffer);
    // Request Speech Service
    request
        .post('http://127.0.0.1:1221/stt')
        .type('form')
        .send({
            "buffer": buffer.toString('base64')
        })
        .then(res => {
            if (res.ok && 'result' in res.body) {
                const result = res.body.result;
                console.log(`Command: ${result}`);
                for (const index in sockets) {
                    if (sockets.hasOwnProperty(index)) {
                        const socket = sockets[index];
                        if (socket.writable) {
                            socket.write(result + "\n");
                        }
                    }
                }
            } else {
                console.error(res.body);
            }
        })
        .catch(err => {
            console.error(err);
        });
}

// When silence event is triggered
detector.on('silence', function () {
    if (catchWord) {
        catchWord = false;
        console.log('end', time());
        recognize();
        bufferArray = [];
    }
});

// When sound event is triggered
detector.on('sound', function (buffer) {
    // <buffer> contains the last chunk of the audio that triggers the "sound"
    // event. It could be written to a wav stream.
    if (catchWord) {
        console.log("recording", time());
        bufferArray.push(buffer);
        if ((time() - startTime) > timeout) {
            catchWord = false;
            console.log('timeout', time());
            recognize();
            bufferArray = [];
        }
    }
});

// When error event is triggered
detector.on('error', function (error) {
    console.error(error);
});

// When hotword event is triggered
detector.on('hotword', function (index, hotword, buffer) {
    // <buffer> contains the last chunk of the audio that triggers the "hotword"
    // event. It could be written to a wav stream. You will have to use it
    // together with the <buffer> in the "sound" event if you want to get audio
    // data after the hotword.
    // console.log(buffer);
    startTime = time();
    console.log('hotword', index, hotword, startTime);
    bufferArray = [buffer];
    catchWord = true;
    // Play the beginSound
    cp.spawn('cvlc', ['--play-and-exit', beginSound]);
    //outputFileStream.write(buffer)
});

record
    .start({
        threshold: 0,
        verbose: false,
        silence: 3
    })
    .on('error', console.error)
    .pipe(detector);
