// Imports the Google Cloud client library
const fs = require('fs');
const speech = require('@google-cloud/speech');

// Creates a client
const client = new speech.SpeechClient();

const filename = 'output.wav';
const encoding = 'LINEAR16';
const sampleRateHertz = 16000;
const languageCode = 'en-GB';

const config = {
    encoding: encoding,
    sampleRateHertz: sampleRateHertz,
    languageCode: languageCode,
    model: 'command_and_search',
    useEnhanced: true
};
const audio = {
    content: fs.readFileSync(filename).toString('base64'),
};

const request = {
    config: config,
    audio: audio,
};

// Detects speech in the audio file
client.recognize(request, function (error, response) {
    if (error) {
        console.error(error);
        return
    }
    const transcription = response.results
        .map(result => result.alternatives[0].transcript)
        .join('.').trim();
    console.log(`Transcription: ${transcription}`);
});
