# Human-Robot Interaction through Speech

- Author: Chengke Sun
- Email: sc17cs@leeds.ac.uk

## Speech to Text module
A command recognition module based on a hotword detection engine. This module converts the voice data into a string.

## License
This project is licensed under the [MIT License](LICENSE)

## How to run in a real TIAGo
Install all dependencies and then run the following command.
```
npm run real
```